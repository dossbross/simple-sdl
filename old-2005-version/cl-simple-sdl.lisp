;;; CL-SPGUI - A Common Lisp Simple Portable GUI
;;;
;;; #Date#: Fri Jul 28 16:13:36 2006


(in-package :common-lisp)


(defpackage :cl-simple-sdl
  (:use :common-lisp)
  (:export :window :image :text :line
	   :*loop-delay*
	   :open-window :close-window
	   :start-main-loop :end-main-loop
	   :pos-x :pos-y :anchor-x :anchor-y :pos-dx :pos-dy
	   :coords :width :height  :view
	   :mouse-x :mouse-y :last-mouse-x :last-mouse-y
	   :mouse-dx :mouse-dy
	   :mouse-button-1 :mouse-button-2 :mouse-button-3
	   :mouse-button-4 :mouse-button-5
	   :key-press
	   :load-image :show :hide
	   :put-text :draw-line :color :data
	   :find-file
	   :set-coords
	   :mouse-in :last-mouse-in
	   :move-to-delta :move-point-to
	   :load-sound :play-sound :halt-sound :set-volume))

(in-package :cl-simple-sdl)

(defparameter *cl-spgui-base-dir* (directory-namestring *load-truename*))
(defparameter *cl-spgui-object-stack* nil)

;;; Low level class definition

(defclass window ()
  ((title :initarg :title :initform "Noname" :accessor title)
   (width :initarg :width :initform 640 :accessor width)
   (height :initarg :height :initform 480 :accessor height)
   (color :initarg :color :initform nil)
   ;; It's to the backend to fill those fields
   (mouse-x :initform 0 :accessor mouse-x)
   (mouse-y :initform 0 :accessor mouse-y)
   (mouse-button-1 :initform nil :accessor mouse-button-1)
   (mouse-button-2 :initform nil :accessor mouse-button-2)
   (mouse-button-3 :initform nil :accessor mouse-button-3)
   (mouse-button-4 :initform nil :accessor mouse-button-4)
   (mouse-button-5 :initform nil :accessor mouse-button-5)
   (last-mouse-x :initform 0 :accessor last-mouse-x)
   (last-mouse-y :initform 0 :accessor last-mouse-y)
   (mouse-dx :initform 0 :accessor mouse-dx)
   (mouse-dy :initform 0 :accessor mouse-dy)
   (init-fun :initarg :init-fun :initform nil :accessor init-fun)
   (main-loop-fun :initarg :main-loop-fun :initform nil
		  :accessor main-loop-fun)
   (end-fun :initarg :end-fun :initform nil :accessor end-fun)
   (data :accessor data)))

(defclass graphic-object ()
  ((window :initarg :window
	   :initform (error "Error, please, set the object window keyword")
	   :accessor window)
   (pos-x :initarg :pos-x :initform 0 :accessor pos-x)
   (pos-y :initarg :pos-y :initform 0 :accessor pos-y)
   (anchor-x :initarg :anchor-x :initform :middle)
   (anchor-y :initarg :anchor-y :initform :middle)
   (width :initarg :width :initform 10 :accessor width)
   (height :initarg :height :initform 10 :accessor height)
   (view :initarg :view :initform nil :accessor view)
   (data :initarg :data :initform nil)))

(defclass image-data ()
  ((data :initarg :data :initform nil :accessor data)
   (width :initarg :width :initform 10 :accessor width)
   (height :initarg :height :initform 10 :accessor height)))
   

(defclass image (graphic-object)
  ())

(defclass text (graphic-object)
  ((text :initarg :text :initform "Not set")
   (color :initarg :color :initform #x000000)))

(defclass line (graphic-object)
  ((coords :initarg :coords :initform '() :accessor coords)
   (color :initarg :color :initform #x000000)))


(defclass sound ()
  ((data :initarg :data :initform nil :accessor data)))




;;; Low level (platform dependent) functions

(defvar *loop-delay* 100)

(defgeneric start-main-loop (window &key wait-event-type fullscreen)
  (:documentation "Start the main loop with a default window"))

(defgeneric end-main-loop (window)
  (:documentation "Stop the main loop"))

(defgeneric open-window (window)
  (:documentation "Open a toplevel window"))

(defgeneric close-window (window)
  (:documentation "Close an opened toplevel window"))

(defgeneric watch-key (window key)
  (:documentation "Watch the event associated with a key"))

(defgeneric load-image (filename width height)
  (:documentation "Load and return an image data"))

(defgeneric show (object)
  (:documentation "Show a graphical object"))

(defgeneric hide (object)
  (:documentation "Hide a graphical object"))

(defgeneric set-coords (object x y &optional other-coords)
  (:documentation "Set object coordinates to (x, y) and optionaly (for lines for example) additionals coordinates)"))

(defgeneric key-press (window key)
  (:documentation "Chech if the key is pressed"))



;;; Sounds functions
(defgeneric load-sound (filename)
  (:documentation "Load and return a new sound"))

(defgeneric play-sound (sound)
  (:documentation "Play a sound"))

(defgeneric halt-sound ()
  (:documentation "Halt all sounds"))

(defgeneric set-sound-volume (vol)
  (:documentation "Set sound volume"))

;;; Low level generic methods and accessors definition

(defmethod data ((object graphic-object))
  (slot-value object 'data))

(defmethod (setf data) (val (object graphic-object))
  (setf (slot-value object 'data) val))

(defgeneric text (object))
(defgeneric (setf text) (val object))

(defmethod text ((obj graphic-object))
  (slot-value obj 'text))

(defmethod color ((text text))
  (slot-value text 'color))

(defmethod color ((line line))
  (slot-value line 'color))

(defgeneric anchor-x (object))
(defgeneric anchor-y (object))
(defgeneric (setf anchor-x) (val object))
(defgeneric (setf anchor-y) (val object))

(defmethod anchor-x ((obj graphic-object))
  (slot-value obj 'anchor-x))

(defmethod anchor-y ((obj graphic-object))
  (slot-value obj 'anchor-y))

(defmethod (setf anchor-x) (val (obj graphic-object))
  (setf (slot-value obj 'anchor-x) val))

(defmethod (setf anchor-y) (val (obj graphic-object))
  (setf (slot-value obj 'anchor-y) val))


(defgeneric val (object))
(defgeneric (setf val) (val object))


;;; Highlevel GUI
(defgeneric mouse-in (object)
  (:documentation "Return T when the mouse is in the object"))

(defgeneric last-mouse-in (object)
  (:documentation "Return T when the mouse was in the object at the previous loop time"))

(defgeneric real-pos-x (object)
  (:documentation "Return the X position of object after placement (:left :middle :right)"))

(defgeneric real-pos-y (object)
  (:documentation "Return the Y position of object after placement (:top :middle :bottom)"))


(defgeneric gui-object-action (object)
  (:documentation "Perform the object action on each loop"))

(defgeneric activate (object &optional do-action)
  (:documentation "activate an object (ex: push a button)"))

(defgeneric deactivate (object &optional do-action)
  (:documentation "Deactivate an object (ex: release a button)"))

(defgeneric toggle (object &optional do-action)
  (:documentation "Toggle an object"))



;;; Implementation

(defun funcall-when-bind (fun &rest args)
  (when fun
    (apply fun args)))

(defun find-file (base-dir list-dir name type)
  (let ((path (make-pathname :host (pathname-host base-dir)
			     :device (pathname-device base-dir)
			     :directory (append (pathname-directory base-dir)
						list-dir)
			     :name name
			     :type type)))
    ;;(format t "Try to open ~A~%" path)
    (namestring path)))


(defmethod mouse-in ((obj graphic-object))
  (and (view obj)
       (<= (real-pos-x obj) (mouse-x (window obj))
	   (+ (real-pos-x obj) (width obj)))
       (<= (real-pos-y obj) (mouse-y (window obj))
	   (+ (real-pos-y obj) (height obj)))))

(defmethod last-mouse-in ((obj graphic-object))
  (and (view obj)
       (<= (real-pos-x obj) (last-mouse-x (window obj))
	   (+ (real-pos-x obj) (width obj)))
       (<= (real-pos-y obj) (last-mouse-y (window obj))
	   (+ (real-pos-y obj) (height obj)))))

(defmethod mouse-in ((line line))
  (unless (view line)
    (return-from mouse-in nil))
  (when (and (<= (- (pos-x line) 5) (mouse-x (window line))
		 (+ (pos-x line) 5))
	     (<= (- (pos-y line) 5) (mouse-y (window line))
		 (+ (pos-y line) 5)))
    (return-from mouse-in 0))
  (do ((c (coords line) (cddr c))
       (i 1 (1+ i)))
      ((null c))
    (when (and (<= (- (first c) 5) (mouse-x (window line)) (+ (first c) 5))
	       (<= (- (second c) 5) (mouse-y (window line)) (+ (second c) 5)))
      (return-from mouse-in i))))

(defmethod last-mouse-in ((line line))
  (unless (view line)
    (return-from last-mouse-in nil))
  (when (and (<= (- (pos-x line) 5) (last-mouse-x (window line))
		 (+ (pos-x line) 5))
	     (<= (- (pos-y line) 5) (last-mouse-y (window line))
		 (+ (pos-y line) 5)))
    (return-from last-mouse-in 0))
  (do ((c (coords line) (cddr c))
       (i 1 (1+ i)))
      ((null c))
    (when (and (<= (- (first c) 5) (last-mouse-x (window line))
		   (+ (first c) 5))
	       (<= (- (second c) 5) (last-mouse-y (window line))
		   (+ (second c) 5)))
      (return-from last-mouse-in i))))


(defgeneric move-to-delta (object dx dy)
  (:documentation "Move an object from (x, y) to (x+dx, y+dy)"))

(defmethod move-to-delta ((line line) dx dy)
  (set-coords line
	      (+ (pos-x line) dx)
	      (+ (pos-y line) dy)
	      (let ((i 0))
		(mapcar #'(lambda (x)
			    (if (oddp (incf i))
				(+ x dx)
				(+ x dy)))
			(coords line)))))

(defgeneric move-point-to (object p x y)
  (:documentation "Move the point 'p' to (x, y)"))

(defmethod move-point-to ((line line) p x y)
  (when (numberp p)
    (if (zerop p)
	(set-coords line x y (coords line))
	(progn
	  (setf (elt (coords line) (* 2 (1- p))) x
		(elt (coords line) (1+ (* 2 (1- p)))) y)
	  (set-coords line
		      (pos-x line)
		      (pos-y line)
		      (coords line))))))

(defun do-action-on-gui-object ()
  "Do actions for all gui objects (must be call in the
implementation main loop)"
  (dolist (obj *cl-spgui-object-stack*)
    (gui-object-action obj)))


(defmethod real-pos-x ((obj graphic-object))
  (ecase (anchor-x obj)
    (:left (pos-x obj))
    (:middle (truncate (- (pos-x obj) (/ (width obj) 2))))
    (:right (- (pos-x obj) (width obj)))))

(defmethod real-pos-y ((obj graphic-object))
  (ecase (anchor-y obj)
    (:top (pos-y obj))
    (:middle (truncate (- (pos-y obj) (/ (height obj) 2))))
    (:bottom (- (pos-y obj) (height obj)))))



;;; C part

(defparameter *sdl-base-dir* (directory-namestring *load-truename*))
(defparameter *sdl-lib-pathname*
  (namestring (make-pathname :host (pathname-host *sdl-base-dir*)
			     :device (pathname-device *sdl-base-dir*)
			     :directory (pathname-directory *sdl-base-dir*)
			     :name "libsimple-sdl"
			     :type "so")))


;;; C wrapper...
#+CLISP
(defmacro ffi-def-function (name c-name args return-type lib)
  `(ffi:def-call-out ,name
    (:name ,c-name)
    (:arguments ,@(loop for i in args
			collect (list (first i)
				      (intern (string-upcase
					       (string (case (second i)
							 (:cstring :c-string)
							 (t (second i)))))
					      :ffi))))
    (:return-type ,(intern (string-upcase (string return-type)) :ffi))
    (:language :STDC)
    (:library ,lib)))

#+CLISP
(defmacro ffi-def-var (name c-name type lib)
  `(ffi:def-c-var ,name
    (:name ,c-name)
    (:type ,(intern (string-upcase (string type)) :ffi))
    (:library ,lib)))

#+UFFI
(defmacro ffi-def-function (name c-name args return-type lib)
  `(uffi:def-function (,c-name ,name)
    ,args
    :returning ,return-type
    :module ,lib))

#+UFFI
(defmacro ffi-def-var (name c-name type lib)
  `(uffi:def-foreign-var (,c-name ,name) ,type ,lib))


(format t "Loading: ~A~%" *sdl-lib-pathname*)

#+UFFI
(uffi:load-foreign-library *sdl-lib-pathname*
			   :module "libsimple-sdl.so"
			   :supporting-libraries '("c"))


;;; CMUCL work around ???? absolutly unuseful...
#+CMU (uffi:def-enum enum-ignored_t (:IGNORED-1 :IGNORED-2 :IGNORED-3))


#+CLISP
(progn
  (defvar *pwd* (ext:cd))
  (ext:cd (make-pathname :host (pathname-host *sdl-base-dir*)
			 :device (pathname-device *sdl-base-dir*)
			 :directory (pathname-directory *sdl-base-dir*))))

(ffi-def-function testfun "testfun" ((x :int)) :int "libsimple-sdl.so")
(ffi-def-var test-var "test_var" :int "libsimple-sdl.so")


(ffi-def-var sdl-done "sdl_done" :int "libsimple-sdl.so")
;;(ffi-def-var sdl-mouse-button-1 "sdl_mouse_button_1" :int "libsimple-sdl.so")
;;(ffi-def-var sdl-mouse-button-2 "sdl_mouse_button_2" :int "libsimple-sdl.so")
;;(ffi-def-var sdl-mouse-button-3 "sdl_mouse_button_3" :int "libsimple-sdl.so")
;;(ffi-def-var sdl-mouse-button-4 "sdl_mouse_button_4" :int "libsimple-sdl.so")
;;(ffi-def-var sdl-mouse-button-5 "sdl_mouse_button_5" :int "libsimple-sdl.so")
(ffi-def-var sdl-mouse-x "sdl_mouse_x" :int "libsimple-sdl.so")
(ffi-def-var sdl-mouse-y "sdl_mouse_y" :int "libsimple-sdl.so")

(ffi-def-var sdl-escape-key "sdl_escape_key" :int "libsimple-sdl.so")
(ffi-def-var sdl-up-key "sdl_up_key" :int "libsimple-sdl.so")
(ffi-def-var sdl-right-key "sdl_right_key" :int "libsimple-sdl.so")
(ffi-def-var sdl-left-key "sdl_left_key" :int "libsimple-sdl.so")
(ffi-def-var sdl-down-key "sdl_down_key" :int "libsimple-sdl.so")

(ffi-def-function sdl-init "sdl_init"
		  ((title :cstring) (width :int) (height :int)
		   (fullscreen :int) (color :int))
		  :int "libsimple-sdl.so")

(ffi-def-function sdl-close "sdl_close" () :int "libsimple-sdl.so")

(ffi-def-function sdl-poll-event "sdl_poll_event" ((wait_ms :int))
		  :int "libsimple-sdl.so")
(ffi-def-function sdl-wait-event "sdl_wait_event" () :int "libsimple-sdl.so")
(ffi-def-function sdl-wait-event-no-motion "sdl_wait_event_no_motion" ()
		  :int "libsimple-sdl.so")
(ffi-def-function sdl-flip-screen "sdl_flip_screen" () :int "libsimple-sdl.so")
(ffi-def-function sdl-show-background "sdl_show_background" ()
		  :int "libsimple-sdl.so")
(ffi-def-function sdl-set-background-color "sdl_set_background_color" ((color :int))
		  :int "libsimple-sdl.so")

(ffi-def-function sdl-key-press "sdl_key_press" ((key :int))
		  :int "libsimple-sdl.so")

(ffi-def-function sdl-load-image "sdl_load_image" ((filename :cstring))
		  :int "libsimple-sdl.so")
(ffi-def-function sdl-put-image "sdl_put_image"
		  ((image :int) (x :int) (y :int)) :int "libsimple-sdl.so")

(ffi-def-function sdl-open-font "sdl_open_font"
		  ((filename :cstring) (size :int)) :int "libsimple-sdl.so")
(ffi-def-function sdl-put-text "sdl_put_text"
		  ((x :int) (y :int) (string :cstring) (color :int))
		  :int "libsimple-sdl.so")
(ffi-def-function sdl-text-width "sdl_text_width"
		  ((string :cstring)) :int "libsimple-sdl.so")
(ffi-def-function sdl-text-height "sdl_text_height"
		  ((string :cstring)) :int "libsimple-sdl.so")

(ffi-def-function sdl-put-line "sdl_put_line"
		  ((x1 :int) (y1 :int) (x2 :int) (y2 :int) (color :int))
		  :int "libsimple-sdl.so")


(ffi-def-function sdl-load-sound "sdl_load_sound"
		  ((filename :cstring))
		  :int "libsimple-sdl.so")

(ffi-def-function sdl-play-sound "sdl_play_sound"
		  ((sound :int))
		  :int "libsimple-sdl.so")

(ffi-def-function sdl-halt-sound "sdl_halt_sound"
		  () :int "libsimple-sdl.so")

(ffi-def-function sdl-set-sound-volume "sdl_set_sound_volume"
		  ((volume :int))
		  :int "libsimple-sdl.so")

(ffi-def-function sdl-get-ticks "sdl_get_ticks"
		  () #+UFFI :unsigned-int #+CLISP :uint32 "libsimple-sdl.so")

#+CLISP
(ext:cd *pwd*)

;;; Main code
(defun i->b (x)
  "Convert integer to boolean"
  (= x 1))

(defvar *done* nil)
(defvar *view-object-stack* nil)
(defvar *have-to-display* t)

(defun display-stack-object ()
  (dolist (obj *view-object-stack*)
    (typecase obj
      (image (sdl-put-image (data (data obj))
			    (real-pos-x obj) (real-pos-y obj)))
      (text (sdl-put-text (real-pos-x obj) (real-pos-y obj)
			  (text obj) (color obj)))
      (line (sdl-put-line (pos-x obj) (pos-y obj)
			  (first (coords obj)) (second (coords obj))
			  (color obj))
	    (do ((i (coords obj) (cddr i)))
		((null (third i)))
	      (sdl-put-line (first i) (second i)
			    (third i) (fourth i) (color obj)))))))


(defun sdl-keycode->char (key)
  (if (>= key 0)
      key
      nil))


(defmethod start-main-loop ((window window) &key (wait-event-type :wait) (fullscreen nil))
  (labels ((set-mouse-button (button-slot sdl-button)
	     (if (i->b sdl-button)
		 (setf (slot-value window button-slot)
		       (if (slot-value window button-slot) :motion :click))
		 (setf (slot-value window button-slot)  nil))))
    (sdl-init (title window) (width window) (height window)
	      (if fullscreen 1 0)
	      (color window))
    (sdl-open-font (find-file *cl-spgui-base-dir*
			      '("font") "Vera" "ttf") 12)
    (when (init-fun window)
      (funcall (init-fun window) window))
    (setf *loop-delay* (/ *loop-delay* 1000))
    (let (wait-event-fun wait-event-args)
      (ecase wait-event-type
	(:wait (setf wait-event-fun #'sdl-wait-event
		     wait-event-args '()))
	(:poll (setf wait-event-fun #'sdl-poll-event
		     wait-event-args (list (* *loop-delay* 1000))))
	(:wait-no-motion (setf wait-event-fun #'sdl-wait-event-no-motion
			       wait-event-args '())))
      (loop while (not *done*) do
	    (apply wait-event-fun wait-event-args)
	    (setf *done* (i->b sdl-done)
		  (last-mouse-x window) (mouse-x window)
		  (last-mouse-y window) (mouse-y window)
		  (mouse-x window) sdl-mouse-x
		  (mouse-y window) sdl-mouse-y
		  (mouse-dx window) (- (mouse-x window) (last-mouse-x window))
		  (mouse-dy window) (- (mouse-y window) (last-mouse-y window)))
	    (set-mouse-button 'mouse-button-1 sdl-mouse-button-1)
	    (set-mouse-button 'mouse-button-2 sdl-mouse-button-2)
	    (set-mouse-button 'mouse-button-3 sdl-mouse-button-3)
	    (set-mouse-button 'mouse-button-4 sdl-mouse-button-4)
	    (set-mouse-button 'mouse-button-5 sdl-mouse-button-5)
	    (do-action-on-gui-object)
	    (unless *done*
	      (funcall (main-loop-fun window) window)
	      (when *have-to-display*
		(unless *done*
		  (sdl-show-background)
		  (display-stack-object)
		  (setf *have-to-display* nil)
		  (sdl-flip-screen))
		(sleep *loop-delay*)))))))

(defmethod end-main-loop ((window window))
  (when (end-fun window)
    (funcall (end-fun window) window))
  (sdl-close)
  (setf *done* t))

(defmethod open-window ((window window))
  (declare (ignore window)))

(defmethod close-window ((window window))
  (declare (ignore window)))

(defmethod key-press ((window window) key)
  (typecase key
    (character (i->b (sdl-key-press (char-code (char-downcase key)))))
    (keyword
     (ecase key
       (:escape (i->b (sdl-key-press sdl-escape-key)))
       (:up (i->b (sdl-key-press sdl-up-key)))
       (:down (i->b (sdl-key-press sdl-down-key)))
       (:left (i->b (sdl-key-press sdl-left-key)))
       (:right (i->b (sdl-key-press sdl-right-key)))))))

(defmethod (setf color) (val (window window))
  (sdl-set-background-color val))

(defmethod color ((window window))
  (slot-value window 'color))

;;; Image part
(defmethod load-image (filename width height)
  (assert (probe-file filename) (filename)
	  "Error loading ~S" filename)
  (make-instance 'image-data
		 :width width :height height
		 :data (sdl-load-image filename)))

(defmethod initialize-instance :after ((image image) &rest rest)
  (declare (ignore rest))
  (setf (width image) (width (data image))
	(height image) (height (data image))))

(defmethod (setf data) (val (image image))
  (setf *have-to-display* t)
  (setf (width image) (width val)
	(height image) (height val)
	(slot-value image 'data) val))


(defmethod show ((image image))
  (setf *have-to-display* t)
  (setf (view image) t)
  (setf *view-object-stack*
	(append (remove image *view-object-stack*) (list image)))
  image)

(defmethod hide ((image image))
  (setf *have-to-display* t)
  (setf (view image) nil)
  (setf *view-object-stack* (remove image *view-object-stack*))
  image)


(defmethod set-coords ((image image) x y &optional other-coords)
  (declare (ignore other-coords))
  (setf *have-to-display* t)
  (setf (pos-x image) x
	(pos-y image) y))


;;; Text part
(defmethod set-coords ((text text) x y &optional other-coords)
  (declare (ignore other-coords))
  (setf *have-to-display* t)
  (setf (pos-x text) x
	(pos-y text) y))

(defmethod show ((text text))
  (setf *have-to-display* t)
  (setf (view text) t)
  (setf (width text) (sdl-text-width (text text))
	(height text) (sdl-text-height (text text)))
  ;;(format t "Text ~A  w = ~A   h = ~A~%"
  ;;	  (text text) (width text) (height text))
  (setf *view-object-stack*
	(append (remove text *view-object-stack*) (list text)))
  text)

(defmethod hide ((text text))
  (setf *have-to-display* t)
  (setf (view text) nil)
  (setf *view-object-stack* (remove text *view-object-stack*))
  text)

(defmethod (setf text) (val (text text))
  (setf *have-to-display* t)
  (setf (slot-value text 'text) val
	(width text) (sdl-text-width val)))
  ;;(format t "Text ~A  w = ~A   h = ~A~%"
  ;;  (text text) (width text) (height text)))

(defmethod (setf color) (val (text text))
  (setf *have-to-display* t)
  (setf (slot-value text 'color) val))

;;; Lines Part
(defmethod set-coords ((line line) x y &optional other-coords)
  (setf *have-to-display* t)
  (setf (pos-x line) x
	(pos-y line) y
	(coords line) other-coords))

(defmethod show ((line line))
  (setf *have-to-display* t)
  (setf (view line) t)
  (setf *view-object-stack*
	(append (remove line *view-object-stack*) (list line)))
  line)

(defmethod hide ((line line))
  (setf *have-to-display* t)
  (setf (view line) nil)
  (setf *view-object-stack* (remove line *view-object-stack*))
  line)

(defmethod (setf color) (val (line line))
  (setf *have-to-display* t)
  (setf (slot-value line 'color) val))






;;; Sounds functions
(defmethod load-sound (filename)
  (assert (probe-file filename) (filename)
	  "Error loading ~S" filename)
  (make-instance 'sound :data (sdl-load-sound filename)))

(defmethod play-sound ((sound sound))
  (sdl-play-sound (data sound)))

(defmethod halt-sound ()
  (sdl-halt-sound))

(defmethod set-sound-volume (vol)
  (sdl-set-sound-volume vol))

