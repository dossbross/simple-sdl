include simple-sdl.fs

debut


s" ball.png" image: ball-img
s" hk.wav" son: rebond

create ball 100 , 100 , 2 , 2 ,

: x ;
: y cell + ;
: dx 2 cells + ;
: dy 3 cells + ;


: .ball ball-img  ball x @   ball y @   clear aff flip ;

.ball


: rnd-dir 3 random 1+ ;
: change-dx ball dx !  rebond joue ;
: change-dy ball dy !  rebond joue ;

\ : inverse-dx? if ball dx @ negate ball dx ! rebond joue then ;
: bord-droite? ball x @ 750 > if rnd-dir negate change-dx then ;
: bord-gauche? ball x @ 0   < if rnd-dir        change-dx then ;


\ : inverse-dy? if ball dy @ negate ball dy ! rebond joue then ;
: bord-bas?  ball y @ 550 > if rnd-dir negate change-dy then ;
: bord-haut? ball y @ 0   < if rnd-dir        change-dy then ;


: dep-x ball dx @  ball x +! bord-droite? bord-gauche? ;
: dep-y ball dy @  ball y +! bord-haut? bord-bas? ;

: mes-actions  dep-x dep-y .ball  ;

' mes-actions is actions
' attend-temps is attend

boucle


cr fin bye
