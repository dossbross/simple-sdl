CC=gcc
PROG=simple-sdl
FLAGS=-Wall `sdl2-config --cflags --libs` -lSDL2_image -lSDL2_ttf -lSDL2_mixer -I. -fPIC

all: libsimple-sdl.so
	$(CC) $(FLAGS) -o lib$(PROG).so -shared $(PROG).c
	gcc $(FLAGS) -I.  -L. -lsimple-sdl -o test test.c
	g++ $(FLAGS) -I.  -L. -lsimple-sdl -o test++ test.cpp


libsimple-sdl.so: simple-sdl.c simple-sdl.h

standalone:
	$(CC) $(FLAGS) -o $(PROG) $(PROG).c

clean:
	rm -rf *~ *.so *.o test test++
