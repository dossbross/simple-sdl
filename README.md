# simple-sdl

An attempt to shrink sdl library to a small subset easy to write in other languages. 

Supported bindings:  
* Common Lisp
* Forth (gforth) 

# Build

Build the shared library *libsimple-sdl.so*

`
make
`

# Usage

* In Forth
  - `gforth test-simple.fr`
  - `gforth test-balle.fs`
