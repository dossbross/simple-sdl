#include <stdio.h>
#include <stdlib.h>
#include <simple-sdl.h>


/* Please, edit this for your project */
#define MAX_IMAGE 1000
#define MAX_KEY 1000
#define MAX_SOUND 1000

int test_var = 10;

int sdl_done = 0;
/* int sdl_mouse_button_1 = 0; */
/* int sdl_mouse_button_2 = 0; */
/* int sdl_mouse_button_3 = 0; */
/* int sdl_mouse_button_4 = 0; */
/* int sdl_mouse_button_5 = 0; */
int sdl_mouse_x = 0;
int sdl_mouse_y = 0;

int tab_mouse_button[5];

int tab_keys[MAX_KEY];    /* Is this enough ? */

int image_count = -1;
SDL_Surface * tab_images[MAX_IMAGE];

int sound_count = -1;
Mix_Chunk * tab_sounds[MAX_SOUND];


int sdl_escape_key = SDLK_ESCAPE;
int sdl_up_key = SDLK_UP;
int sdl_down_key = SDLK_DOWN;
int sdl_left_key = SDLK_LEFT;
int sdl_right_key = SDLK_RIGHT;

static Uint32 videoflags;
static Uint8  video_bpp;
static SDL_Surface * main_screen;
static SDL_Surface * background;
static TTF_Font * font = NULL;

void putpixel (SDL_Surface *surface, int x, int y, Uint32 pixel);
void set_background_color (SDL_Surface *surface, Uint32 color);
void sdl_show_background (void);

/*===========================================================
 *		Initialisation et fermeture
 ============================================================*/


/* This is a way of telling whether or not to use hardware surfaces */
static Uint32 FastestFlags(Uint32 flags, int width, int height)
{
	const SDL_VideoInfo *info;

	/* Hardware acceleration is only used in fullscreen mode */
	flags |= SDL_FULLSCREEN;

	/* Check for various video capabilities */
	info = SDL_GetVideoInfo();
	if ( info->blit_hw_CC && info->blit_fill ) {
		/* We use accelerated colorkeying and color filling */
		flags |= SDL_HWSURFACE;
	}
	/* If we have enough video memory, and will use accelerated
	   blits directly to it, then use page flippeng.
	 */
	if ( (flags & SDL_HWSURFACE) == SDL_HWSURFACE ) {
		/* Direct hardware blitting without double-buffering
		   causes really bad flickering.
		 */
		if ( info->video_mem > width*height ) {
			flags |= SDL_DOUBLEBUF;
		} else {
			flags &= ~SDL_HWSURFACE;
		}
	}

	/* Return the flags */
	return(flags);
}


void
sdl_init (char * title, int width, int height, int fullscreen, int color)
{
  int i;

  if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
	  fprintf(stderr, "Couldn't initialize SDL: %s\n",SDL_GetError());
	  exit(1);
	}
  atexit(SDL_Quit);

  if ( TTF_Init() < 0 ) {
	fprintf(stderr, "Couldn't initialize TTF: %s\n",SDL_GetError());
	exit(2);
  }
  atexit(TTF_Quit);

  SDL_WM_SetCaption (title, title);

  videoflags = SDL_SWSURFACE | SDL_ANYFORMAT
	| FastestFlags(videoflags, width, height);
  video_bpp = 8;

  if (fullscreen == 0) videoflags ^= SDL_FULLSCREEN;

  /* Set MAXXxMAXY (640x480) video mode */
  if ( (main_screen=SDL_SetVideoMode(width, height, video_bpp, videoflags))
	   == NULL )
	{
	  fprintf(stderr, "Couldn't set %dx%d video mode: %s\n",
		  width, height, SDL_GetError());
	  exit(2);
	}


  /* Sound initialisation */
  if (SDL_Init(SDL_INIT_AUDIO) < 0)
	{
	  fprintf(stderr,
		  "\nWarning: I could not initialize audio!\n"
		  "The Simple DirectMedia error that occured was:\n"
		  "%s\n\n", SDL_GetError());
	}

  /* if (Mix_OpenAudio(22050, AUDIO_S16, 2, 512) < 0) */
  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0)
	{
	  fprintf(stderr,
		  "\nWarning: I could not set up audio for 44100 Hz "
		  "16-bit stereo.\n"
		  "The Simple DirectMedia error that occured was:\n"
		  "%s\n\n", SDL_GetError());
	}
  sdl_set_sound_volume(2);

  // allocate mixing channels
  Mix_AllocateChannels(100);

  /* Background init */
  background = SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCCOLORKEY,
					width, height,
					main_screen->format->BitsPerPixel,
					0,0,0,0);

  set_background_color (background, color);
  sdl_show_background ();

  for (i = 0; i < MAX_KEY; i++)
	tab_keys[i] = 0;

  for (i = 0; i < 5; i++)
	tab_mouse_button[i] = 0;

  return;
}

void
sdl_close (void)
{
  SDL_FreeSurface(main_screen);

  if ( !(font == NULL))
	{
	  TTF_CloseFont(font);
	  font = NULL;
	}

  Mix_CloseAudio();

  SDL_Quit();

  return;
}


void
sdl_show_cursor (void)
{
  SDL_ShowCursor (SDL_ENABLE);

  return;
}

void
sdl_hide_cursor (void)
{
  SDL_ShowCursor (SDL_DISABLE);

  return;
}


void
analyze_event (SDL_Event * event)
{
	if (event->type != 0) {
		printf("event = %d %d\n", event->type, SDL_KEYDOWN);
	}

	switch( event->type )
		{
		case SDL_QUIT : sdl_done = 1; break;

		case SDL_MOUSEBUTTONDOWN :
			if (event->button.button == SDL_BUTTON_LEFT) tab_mouse_button[0] = 1;
			if (event->button.button == SDL_BUTTON_MIDDLE) tab_mouse_button[1] = 1;
			if (event->button.button == SDL_BUTTON_RIGHT) tab_mouse_button[2] = 1;
			if (event->button.button == 4) tab_mouse_button[3] = 1;
			if (event->button.button == 5) tab_mouse_button[4] = 1;
			break;

		case SDL_MOUSEBUTTONUP :
			if (event->button.button == SDL_BUTTON_LEFT) tab_mouse_button[0] = 0;
			if (event->button.button == SDL_BUTTON_MIDDLE) tab_mouse_button[1] = 0;
			if (event->button.button == SDL_BUTTON_RIGHT) tab_mouse_button[2] = 0;
			if (event->button.button == 4) tab_mouse_button[3] = 0;
			if (event->button.button == 5) tab_mouse_button[4] = 0;
			break;

		case SDL_KEYDOWN :
			tab_keys[event->key.keysym.sym] = 1;
			printf ("In C- Key press: %s\n", SDL_GetKeyName (event->key.keysym.sym));
			break;
		case SDL_KEYUP :
			tab_keys[event->key.keysym.sym] = 0;
			break;
		}
}

void
sdl_poll_event (int wait_ms)
{
  SDL_Event event;
  Uint32 start = SDL_GetTicks ();

  while ( ! SDL_PollEvent (&event) && (SDL_GetTicks () - start) < wait_ms)
	{
	  SDL_Delay (2);
	}

  tab_mouse_button[3] = 0;
  tab_mouse_button[4] = 0;
  analyze_event (&event);

  while ( SDL_PollEvent( &event ) )
	{
	  analyze_event (&event);
	}

  SDL_GetMouseState (&sdl_mouse_x, &sdl_mouse_y);

  return;
}


void
sdl_wait_event (void)
{
  SDL_Event event;

  SDL_WaitEvent (&event);

  tab_mouse_button[3] = 0;
  tab_mouse_button[4] = 0;
  analyze_event (&event);

  while ( SDL_PollEvent( &event ) )
	{
	  analyze_event (&event);
	}

  SDL_GetMouseState (&sdl_mouse_x, &sdl_mouse_y);

  return;
}


void
sdl_wait_event_no_motion (void)
{
  SDL_Event event;

  do {
	SDL_WaitEvent (&event);
  } while (event.type == SDL_MOUSEMOTION);

  tab_mouse_button[3] = 0;
  tab_mouse_button[4] = 0;
  analyze_event (&event);

  while ( SDL_PollEvent( &event ) )
	{
	  analyze_event (&event);
	}

  SDL_GetMouseState (&sdl_mouse_x, &sdl_mouse_y);

  return;
}



void
sdl_flip_screen (void)
{
  SDL_Flip(main_screen);

  return;
}


int
sdl_key_press (int key)
{
  return tab_keys[key];
}

int
sdl_key_press_reset (int key)
{
  int val = tab_keys[key];

  tab_keys[key] = 0;

  return val;
}



int
sdl_mouse_button (int button)
{
  return tab_mouse_button[button - 1];
}

int
sdl_get_mouse_x (void)
{
  return sdl_mouse_x;
}


int
sdl_get_mouse_y (void)
{
  return sdl_mouse_y;
}



/*  * Set the pixel at (x, y) to the given value
	* NOTE: The surface must be locked before calling this!  */
void
putpixel (SDL_Surface *surface, int x, int y, Uint32 pixel)
{
  int bpp = surface->format->BytesPerPixel;
  /* Here p is the address to the pixel we want to set */
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

  switch(bpp)
	{
	case 1: *p = pixel; break;
	case 2: *(Uint16 *)p = pixel; break;
	case 3:
	  if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
	{
	  p[0] = (pixel >> 16) & 0xff;
	  p[1] = (pixel >> 8) & 0xff;
	  p[2] = pixel & 0xff;
	} else {
	  p[0] = pixel & 0xff;
	  p[1] = (pixel >> 8) & 0xff;
	  p[2] = (pixel >> 16) & 0xff;
	}
	  break;
	case 4:         *(Uint32 *)p = pixel;         break;
	}
  return;
}

void
set_background_color (SDL_Surface *surface, Uint32 color)
{
  SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format,
					 (color >> 16) & 0xff,
					 (color >> 8) & 0xff,
					 color & 0xff));
  SDL_UpdateRect(surface, 0, 0, 0, 0);

  return;
}


void
sdl_set_background_color (int color)
{
  set_background_color(background, color);

  return;
}

void
sdl_show_background (void)
{
  SDL_BlitSurface(background, NULL, main_screen, NULL);

  return;
}



int
sdl_load_image (char * filename)
{
  image_count += 1;
  if (image_count >= MAX_IMAGE)
	{
	  fprintf (stderr, "Sorry can't load more than %d images\n", MAX_IMAGE);
	  return -1;
	}

  /* printf ("In C - Try to open: %s as image %d\n", filename, image_count); */

  tab_images[image_count] = IMG_Load (filename);

  return image_count;
}

void
sdl_put_image (int image, int x, int y)
{
  SDL_Rect dstrect;

  dstrect.x = x; dstrect.y = y;
  dstrect.w = tab_images[image]->w; dstrect.h = tab_images[image]->h;
  SDL_BlitSurface(tab_images[image], NULL, main_screen, &dstrect);

  return;
}




int
sdl_open_font (char * filename, int ptsize)
{
  if ( !(font == NULL))
	TTF_CloseFont(font);

  font = TTF_OpenFont(filename, ptsize);
  if ( font == NULL ) {
	fprintf(stderr, "Couldn't load %d pt font from %s: %s\n",
		ptsize, filename, SDL_GetError());
	return -1;
  }
  TTF_SetFontStyle(font, TTF_STYLE_NORMAL);

  return 0;
}

void
sdl_put_text (char * string, int x, int y, int forecol)
{
  SDL_Color fg;
  /* SDL_Color bg; */
  SDL_Surface *text;
  SDL_Rect dstrect;

  fg.r = (forecol >> 16) & 0xff;
  fg.g = (forecol >> 8) & 0xff;
  fg.b = forecol & 0xff;

  /* text = TTF_RenderText_Shaded(font, string, fg, bg); */
  text = TTF_RenderText_Solid(font, string, fg);

  if ( text != NULL ) {
	dstrect.x = x;
	dstrect.y = y;
	dstrect.w = text->w;
	dstrect.h = text->h;
	SDL_BlitSurface(text, NULL, main_screen, &dstrect);
	SDL_FreeSurface(text);
  }

  return;
}

int
sdl_text_width (char *string)
{
  SDL_Surface *text;
  SDL_Color fg;
  int ret = 0;

  text = TTF_RenderText_Solid(font, string, fg);

  if ( text != NULL ) {
	ret = text->w;
	SDL_FreeSurface(text);
  }

  return ret;
}


int
sdl_text_height (char *string)
{
  SDL_Surface *text;
  SDL_Color fg;
  int ret = 0;

  text = TTF_RenderText_Solid(font, string, fg);

  if ( text != NULL ) {
	ret = text->h;
	SDL_FreeSurface(text);
  }

  return ret;
}


void
sdl_put_line (int x1, int y1, int x2, int y2, int color)
{
  int i, d;
  float dx, dy;

  Uint32 c = SDL_MapRGB(main_screen->format,
			(color >> 16) & 0xff,
			(color >> 8) & 0xff,
			color & 0xff);

  /* Lock the screen for direct access to the pixels */
  if ( SDL_MUSTLOCK(main_screen) )
	{
	  if ( SDL_LockSurface(main_screen) < 0 )
	{
	  fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
	  return;
	}
	}

  if (x1 < 0) x1 = 0;
  if (x1 >= main_screen->w) x1 = main_screen->w - 1;
  if (x2 < 0) x2 = 0;
  if (x2 >= main_screen->w) x2 = main_screen->w - 1;

  if (y1 < 0) y1 = 0;
  if (y1 >= main_screen->h) y1 = main_screen->h - 1;
  if (y2 < 0) y2 = 0;
  if (y2 >= main_screen->h) y2 = main_screen->h - 1;

  /* Hocwp: This is the slow method: use a more fast algorithm */
  if (abs (x2 - x1) < abs (y2 - y1)) {
	d = abs (y2 - y1);
  } else {
	d = abs (x2 - x1);
  }

  dx = (x2 - x1) / (float) d;
  dy = (y2 - y1) / (float) d;

  for (i = 0; i < d; i++)
	putpixel (main_screen, x1 + dx * i, y1 + dy * i, c);

  if ( SDL_MUSTLOCK(main_screen) )
	{
	  SDL_UnlockSurface(main_screen);
	}

  /* Update just the part of the display that we've changed */
  /*  Hocwp: all the screen is redrawed in the main loop... */
  /* SDL_UpdateRect(main_screen, x1, y1, 1, 1); */
  /*   SDL_UpdateRect(main_screen, x2, y2, 1, 1); */

  return;
}




/* Sound part */
void
sdl_play_sound(int snd)
{
  Mix_PlayChannel(-1, tab_sounds[snd], 0);

  return;
}

void
sdl_halt_sound(void)
{
  Mix_HaltChannel(-1);

  return;
}

void
sdl_set_sound_volume(int volume)
{
  Mix_Volume(-1, volume * MIX_MAX_VOLUME / 3);

  return;
}


int
sdl_load_sound (char * filename)
{
  sound_count += 1;
  if (sound_count >= MAX_SOUND)
	{
	  fprintf (stderr, "Sorry can't load more than %d sounds\n", MAX_SOUND);
	  return -1;
	}

  tab_sounds[sound_count] = Mix_LoadWAV(filename);

  if (tab_sounds[sound_count] == NULL)
	{
	  fprintf(stderr,
		  "\nError: I could not load the sound file:\n"
		  "%s\n"
		  "The Simple DirectMedia error that occured was:\n"
		  "%s\n\n", filename, SDL_GetError());
	}

  return sound_count;
}



unsigned int
sdl_get_ticks (void)
{
  return SDL_GetTicks ();
}



int
testfun (int arg)
{
  int snd;

  printf ("In C - test-fun: arg=%d\n", arg);

  sdl_init ("Just a C Test", 640, 480, 0, 0x700070);

  sdl_open_font ("font/Vera.ttf", 12);

  sdl_put_text ("This is a simple test", 100, 100, 0x00FF00);

  snd = sdl_load_sound ("bomb.wav");

  sdl_flip_screen ();

  sdl_set_sound_volume(1);

  printf ("In C - sdl_get_ticks = %d\n", sdl_get_ticks ());

  while (! sdl_done)
	{
	  sdl_play_sound (snd);
	  sdl_poll_event (3000);
	}

  SDL_FreeSurface(main_screen);
  SDL_Quit ();

  return 10;
}



int
main (int argc, char **argv)
{
  testfun (10);
  return 0;
}
