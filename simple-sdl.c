#include <stdio.h>
#include <stdlib.h>
#include <simple-sdl.h>

///* Please, edit this for your project */
#define MAX_IMAGE 1000
#define MAX_KEY 300
#define MAX_SOUND 300

int sdl_done = 0;
///* int sdl_mouse_button_1 = 0; */
///* int sdl_mouse_button_2 = 0; */
///* int sdl_mouse_button_3 = 0; */
///* int sdl_mouse_button_4 = 0; */
///* int sdl_mouse_button_5 = 0; */
int sdl_mouse_x = 0;
int sdl_mouse_y = 0;

int tab_mouse_button[3];
int tab_keys[MAX_KEY];

int image_count = -1;
SDL_Texture * tab_images[MAX_IMAGE];

int sound_count = -1;
Mix_Chunk * tab_sounds[MAX_SOUND];

SDL_Window * main_screen;
SDL_Renderer *main_renderer;
static TTF_Font * font = NULL;

// /*===========================================================
//  *		Open and close functions
//  ============================================================*/
void sdl_init (int width, int height, int fullscreen) {
	Uint32 videoflags = 0;
	int i;

	sdl_done = 0;

	char *title = "TODO: Move this into a word";

	if ( IMG_Init(IMG_INIT_PNG) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL Image: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(IMG_Quit);

	if ( SDL_Init(SDL_INIT_EVERYTHING) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL: %s\n", SDL_GetError());
		exit(1);
	}
	atexit(SDL_Quit);

	if ( TTF_Init() < 0 ) {
		fprintf(stderr, "Couldn't initialize TTF: %s\n", SDL_GetError());
		exit(2);
	}
	atexit(TTF_Quit);

	if (fullscreen == 1) {
		videoflags ^= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	if ((main_screen = SDL_CreateWindow("Atlast SDL",
										 SDL_WINDOWPOS_UNDEFINED,
										 SDL_WINDOWPOS_UNDEFINED,
										 width, height,
										 videoflags)) == NULL) {
		fprintf(stderr, "Couldn't set %dx%d video mode: %s\n", width, height, SDL_GetError());
		exit(2);
	}

	SDL_SetWindowTitle (main_screen, title);
	SDL_SetWindowInputFocus(main_screen);

	if ((main_renderer = SDL_CreateRenderer(main_screen, -1, 0)) == NULL) {
		fprintf(stderr, "Couldn't create renderer: %s\n", SDL_GetError());
		exit(2);
	}

	/* Sound initialisation */
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
		{
			fprintf(stderr,
					"\nWarning: I could not initialize audio!\n"
					"The Simple DirectMedia error that occured was:\n"
					"%s\n\n", SDL_GetError());
		}

	/* if (Mix_OpenAudio(22050, AUDIO_S16, 2, 512) < 0) */
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0)
		{
			fprintf(stderr,
					"\nWarning: I could not set up audio for 44100 Hz "
					"16-bit stereo.\n"
					"The Simple DirectMedia error that occured was:\n"
					"%s\n\n", SDL_GetError());
		}
	Mix_Volume(-1, 20 * MIX_MAX_VOLUME / 100);

	// allocate mixing channels
	Mix_AllocateChannels(100);

	for (i = 0; i < MAX_KEY; i++) {
		tab_keys[i] = 0;
	}

	for (i = 0; i < 3; i++) {
		tab_mouse_button[i] = 0;
	}
}

void sdl_close () {
	SDL_DestroyRenderer(main_renderer);
	SDL_DestroyWindow(main_screen);

	if ( !(font == NULL)) {
		TTF_CloseFont(font);
		font = NULL;
	}

	Mix_CloseAudio();

	IMG_Quit();
	SDL_Quit();
}

void sdl_delay (int ms) {
	SDL_Delay(ms);
}

void sdl_show_cursor () {
	SDL_ShowCursor (SDL_ENABLE);
}

void sdl_hide_cursor () {
	SDL_ShowCursor (SDL_DISABLE);
}

void set_tab_keys_from_keysym(int keysym, int value) {
	switch(keysym) {
	case SDLK_UP:            tab_keys[27] = value; break;
	case SDLK_DOWN:          tab_keys[28] = value; break;
	case SDLK_LEFT:          tab_keys[29] = value; break;
	case SDLK_RIGHT:         tab_keys[30] = value; break;
	case SDLK_RETURN:        tab_keys[31] = value; break;
	case SDLK_SPACE:         tab_keys[32] = value; break;
	case SDLK_TAB:           tab_keys[33] = value; break;
	case SDLK_LCTRL:         tab_keys[34] = value; break;
	case SDLK_KP_DIVIDE:     tab_keys[35] = value; break;
	case SDLK_KP_MULTIPLY:   tab_keys[36] = value; break;
	case SDLK_KP_MINUS:      tab_keys[37] = value; break;
	case SDLK_KP_PLUS:       tab_keys[38] = value; break;
	case SDLK_KP_ENTER:      tab_keys[39] = value; break;
	case SDLK_KP_1:          tab_keys[40] = value; break;
	case SDLK_KP_2:          tab_keys[41] = value; break;
	case SDLK_KP_3:          tab_keys[42] = value; break;
	case SDLK_KP_4:          tab_keys[43] = value; break;
	case SDLK_KP_5:          tab_keys[44] = value; break;
	case SDLK_KP_6:          tab_keys[45] = value; break;
	case SDLK_KP_7:          tab_keys[46] = value; break;
	case SDLK_KP_8:          tab_keys[47] = value; break;
	case SDLK_KP_9:          tab_keys[48] = value; break;
	case SDLK_KP_0:          tab_keys[49] = value; break;
	case SDLK_KP_PERIOD:     tab_keys[50] = value; break;
	}
}

void analyze_event (SDL_Event * event) {
	switch( event->type ) {
	case SDL_QUIT : sdl_done = 1; break;

	case SDL_MOUSEBUTTONDOWN :
		if (event->button.button == SDL_BUTTON_LEFT) tab_mouse_button[0] = 1;
		if (event->button.button == SDL_BUTTON_MIDDLE) tab_mouse_button[1] = 1;
		if (event->button.button == SDL_BUTTON_RIGHT) tab_mouse_button[2] = 1;
		break;

	case SDL_MOUSEBUTTONUP :
		if (event->button.button == SDL_BUTTON_LEFT) tab_mouse_button[0] = 0;
		if (event->button.button == SDL_BUTTON_MIDDLE) tab_mouse_button[1] = 0;
		if (event->button.button == SDL_BUTTON_RIGHT) tab_mouse_button[2] = 0;
		break;

	case SDL_KEYDOWN :
		printf ("In C- Key press: %s %d 0x%02X\n", SDL_GetKeyName(event->key.keysym.sym), event->key.keysym.sym, event->key.keysym.scancode);
		if (event->key.keysym.sym == SDLK_ESCAPE) {
			sdl_done = 1;
		}

		if (event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z) {
			tab_keys[event->key.keysym.sym - SDLK_a] = 1;
		} else {
			set_tab_keys_from_keysym(event->key.keysym.sym, 1);
		}
		break;
	case SDL_KEYUP :
		printf ("In C- Key release: %s %d 0x%02X\n", SDL_GetKeyName(event->key.keysym.sym), event->key.keysym.sym, event->key.keysym.scancode);
		if (event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z) {
			tab_keys[event->key.keysym.sym - SDLK_a] = 0;
		} else {
			set_tab_keys_from_keysym(event->key.keysym.sym, 0);
		}
		break;
	default:
		break;
	}
}

void sdl_poll_event () {
	SDL_Event event;

	while(SDL_PollEvent(&event)) {
		analyze_event(&event);
	}

	SDL_GetMouseState (&sdl_mouse_x, &sdl_mouse_y);
}

void sdl_present () {
	SDL_RenderPresent(main_renderer);
}


int sdl_key_press (int key) {
	return tab_keys[key];
}

int sdl_mouse_button (int button) {
	return tab_mouse_button[button - 1];
}

int sdl_get_mouse_x (void) {
	return sdl_mouse_x;
}

int sdl_get_mouse_y (void) {
	return sdl_mouse_y;
}

void sdl_background (int red, int green, int blue) {
	SDL_SetRenderDrawColor(main_renderer, red, green, blue, 255);
	SDL_RenderClear(main_renderer);
}

int sdl_load_image (char * filename) {
	SDL_Surface * surf = NULL;

	image_count += 1;
	if (image_count >= MAX_IMAGE) {
		image_count -= 1;
		fprintf (stderr, "Sorry can't load more than %d images\n", MAX_IMAGE);
		return -1;
	}

	printf ("In C - Opening: %s as image %d\n", filename, image_count);

	surf = IMG_Load (filename);
	if (surf == NULL) {
		image_count -= 1;
		fprintf(stderr, "Can't load image %s - %s\n", filename, SDL_GetError());
		return -1;
	}

	tab_images[image_count] = SDL_CreateTextureFromSurface(main_renderer, surf);
	if (tab_images[image_count] == NULL) {
		image_count -= 1;
		fprintf(stderr, "Can't load image %s - %s\n", filename, SDL_GetError());
		return -1;
	}
	SDL_FreeSurface(surf);

	return image_count;
}

void sdl_put_image (int image, int x, int y) {
	SDL_Rect dstrect;
	int w, h;

	SDL_QueryTexture(tab_images[image], NULL, NULL, &w, &h);
	dstrect.x = x; dstrect.y = y;
	dstrect.w = w; dstrect.h = h;
	SDL_RenderCopy(main_renderer, tab_images[image], NULL, &dstrect);
}

//,-----
//| Font part
//`-----
int sdl_open_font (char * filename, int ptsize) {
	if ( !(font == NULL)) {
		TTF_CloseFont(font);
	}

	font = TTF_OpenFont(filename, ptsize);
	if ( font == NULL ) {
		fprintf(stderr, "Couldn't load %d pt font from %s: %s\n",
				ptsize, filename, SDL_GetError());
		return -1;
	}
	TTF_SetFontStyle(font, TTF_STYLE_NORMAL);

	return 0;
}

void sdl_put_text (char * string, int x, int y, int forecol) {
	SDL_Color fg;
	/* SDL_Color bg; */
	SDL_Surface *text;
	SDL_Rect dstrect;
	SDL_Texture *texture;

	fg.r = (forecol >> 16) & 0xff;
	fg.g = (forecol >> 8) & 0xff;
	fg.b = forecol & 0xff;

	/* text = TTF_RenderText_Shaded(font, string, fg, bg); */
	text = TTF_RenderText_Solid(font, string, fg);

	if ( text != NULL ) {
		dstrect.x = x;
		dstrect.y = y;
		dstrect.w = text->w;
		dstrect.h = text->h;
		//SDL_BlitSurface(text, NULL, main_renderer, &dstrect);
		texture = SDL_CreateTextureFromSurface(main_renderer, text);
		//dstrect.x = x; dstrect.y = y;
		//dstrect.w = w; dstrect.h = h;
		//printf("From C: x=%d, y=%d, w=%d, h=%d", x, y, w, h);
		//printf("Load %p\n", tab_images[image]);
		SDL_RenderCopy(main_renderer, texture, NULL, &dstrect);
		SDL_FreeSurface(text);
	}

	return;
}

int sdl_text_width (char *string) {
	SDL_Surface *text;
	SDL_Color fg;
	int ret = 0;

	text = TTF_RenderText_Solid(font, string, fg);

	if ( text != NULL ) {
		ret = text->w;
		SDL_FreeSurface(text);
	}

	return ret;
}


int sdl_text_height (char *string) {
	SDL_Surface *text;
	SDL_Color fg;
	int ret = 0;

	text = TTF_RenderText_Solid(font, string, fg);

	if ( text != NULL ) {
		ret = text->h;
		SDL_FreeSurface(text);
	}

	return ret;
}

//,-----
//| Sound part
//`-----
void sdl_play_sound(int snd) {
	Mix_PlayChannel(-1, tab_sounds[snd], 0);
}

void sdl_halt_sound() {
	Mix_HaltChannel(-1);
}

void sdl_set_sound_volume(int percent) {
	Mix_Volume(-1, percent * MIX_MAX_VOLUME / 100);
}


int sdl_load_sound (char *filename) {
	sound_count += 1;
	if (sound_count >= MAX_SOUND)
		{
			fprintf (stderr, "Sorry can't load more than %d sounds\n", MAX_SOUND);
			sound_count -= 1;
			return -1;
		}

	tab_sounds[sound_count] = Mix_LoadWAV(filename);
	if (tab_sounds[sound_count] == NULL) {
		fprintf(stderr,
				"\nError: I could not load the sound file:\n"
				"%s\n"
				"The Simple DirectMedia error that occured was:\n"
				"%s\n\n", filename, SDL_GetError());
		sound_count -= 1;
		return -1;
	}
	return sound_count;
}

unsigned int sdl_get_ticks () {
	return SDL_GetTicks ();
}

unsigned int sdl_is_done () {
	return (sdl_done != 0);
}



int testfun (int arg) {
	int snd;

	printf ("In C - test-fun: arg=%d\n", arg);

	sdl_init (640, 480, 0);

	sdl_background(10, 10, 10);

	sdl_open_font ("font/Vera.ttf", 12);

	sdl_put_text ("This is a simple test", 100, 100, 0x00FF00);

	sdl_set_sound_volume(10);
	snd = sdl_load_sound ("bomb.wav");

	sdl_present ();

	printf ("In C - sdl_get_ticks = %d\n", sdl_get_ticks ());

	while (! sdl_done) {
		sdl_play_sound (snd);
		sdl_poll_event (3000);
	}

	sdl_close();

	return 10;
}



int main (int argc, char **argv) {
  testfun (10);
  return 0;
}
