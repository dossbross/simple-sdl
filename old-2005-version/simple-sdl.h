#ifndef __SIMPLE_SDL_H
#define __SIMPLE_SDL_H

#include <unistd.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>


#ifdef __cplusplus
extern "C" {
#endif

  extern int testfun (int arg);

  extern void sdl_init (char * title, int width, int height,
		       int fullscreen, int color);

  extern void sdl_close (void);

  extern void sdl_show_cursor (void);
  extern void sdl_hide_cursor (void);

  extern void sdl_poll_event (int wait_ms);
  extern void sdl_wait_event (void);
  extern void sdl_wait_event_no_motion (void);

  extern void sdl_flip_screen (void);

  extern int sdl_key_press (int key);
  extern int sdl_key_press_reset (int key);

  extern int sdl_mouse_button (int button);
  extern int sdl_get_mouse_x (void);
  extern int sdl_get_mouse_y (void);

  extern void sdl_set_background_color (int color);
  extern void sdl_show_background (void);

  extern int sdl_load_image (char * filename);
  extern void sdl_put_image (int image, int x, int y);

  extern int sdl_open_font (char * filename, int ptsize);
  extern void sdl_put_text (char * string, int x, int y, int forecol);
  extern int sdl_text_width (char *string);
  extern int sdl_text_height (char *string);

  extern void sdl_put_line (int x1, int y1, int x2, int y2, int color);


  /* Sound part */
  extern void sdl_play_sound(int snd);
  extern void sdl_halt_sound(void);
  extern void sdl_set_sound_volume(int volume);
  extern int sdl_load_sound (char * filename);

  extern unsigned int sdl_get_ticks (void);

#ifdef __cplusplus
}
#endif

#endif
