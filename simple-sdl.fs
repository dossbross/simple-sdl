include lib.fs
include random.fs

\ We need to make some C strings, so we include the forth to c
\ routine from above:
: ftoc ( c-addr u -- 'c-addr )
	here dup >r over allot swap cmove 0 c, align r> ;

: z" ( -- )  postpone s" postpone ftoc ; immediate

library libsdl ./libsimple-sdl.so

libsdl sdl-init int int int (void) sdl_init
libsdl sdl-close (void) sdl_close

libsdl sdl-delay int (void) sdl_delay
libsdl sdl-show-cursor (void) sdl_show_cursor
libsdl sdl-hide-cursor (void) sdl_hide_cursor

libsdl sdl-testfun int (int) testfun

libsdl sdl-poll-event (void) sdl_poll_event

libsdl sdl-present (void) sdl_present

libsdl sdl-key-press int (int) sdl_key_press

libsdl sdl-mouse-button int (int) sdl_mouse_button
libsdl sdl-mouse-x (int) sdl_get_mouse_x
libsdl sdl-mouse-y (int) sdl_get_mouse_y

: sdl-mouse ( -- x y) sdl-mouse-x sdl-mouse-y ;

libsdl sdl-background int int int (void) sdl_background

libsdl sdl-load-image ptr (int) sdl_load_image
libsdl sdl-put-image int int int (void) sdl_put_image

libsdl sdl-open-font ptr int (int) sdl_open_font
libsdl sdl-put-text ptr int int int (void) sdl_put_text
libsdl sdl-text-width ptr (int) sdl_text_width
libsdl sdl-text-height ptr (int) sdl_text_height

libsdl sdl-load-sound ptr (int) sdl_load_sound
libsdl sdl-play-sound int (void) sdl_play_sound
libsdl sdl-halt-sound (void) sdl_halt_sound
libsdl sdl-set-sound-volume int (void) sdl_set_sound_volume

libsdl sdl-get-ticks (int) sdl_get_ticks
libsdl sdl-done? (int) sdl_is_done

: :image  ( addr n --)
  ftoc sdl-load-image create ,
  does> @ ;

: :sound    ( addr n --)
  ftoc sdl-load-sound create ,
  does> @ ;

0  constant SDLK_a
1  constant SDLK_b
2  constant SDLK_c
3  constant SDLK_d
4  constant SDLK_e
5  constant SDLK_f
6  constant SDLK_g
7  constant SDLK_h
8  constant SDLK_i
9  constant SDLK_j
10 constant SDLK_k
11 constant SDLK_l
12 constant SDLK_m
13 constant SDLK_n
14 constant SDLK_o
15 constant SDLK_p
16 constant SDLK_q
17 constant SDLK_r
18 constant SDLK_s
19 constant SDLK_t
20 constant SDLK_u
21 constant SDLK_v
22 constant SDLK_w
23 constant SDLK_x
24 constant SDLK_y
25 constant SDLK_z

27 constant SDLK_UP
28 constant SDLK_DOWN
29 constant SDLK_LEFT
30 constant SDLK_RIGHT
31 constant SDLK_RETURN
32 constant SDLK_SPACE
33 constant SDLK_TAB
34 constant SDLK_LCTRL
35 constant SDLK_KP_DIVIDE
36 constant SDLK_KP_MULTIPLY
37 constant SDLK_KP_MINUS
38 constant SDLK_KP_PLUS
39 constant SDLK_KP_ENTER
40 constant SDLK_KP_1
41 constant SDLK_KP_2
42 constant SDLK_KP_3
43 constant SDLK_KP_4
44 constant SDLK_KP_5
45 constant SDLK_KP_6
46 constant SDLK_KP_7
47 constant SDLK_KP_8
48 constant SDLK_KP_9
49 constant SDLK_KP_0
50 constant SDLK_KP_PERIOD
