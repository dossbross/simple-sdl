include sound.fs

\
\ Song part
\
init   set-mixer

s" bomb" s" bomb2.wav" add-sound bomb
s" synth" s" synth2.wav" add-sound synth


s" sdo" s" synth2.wav" add-sound sdo
s" sre" s" synth2.wav" ->re add-sound sre
s" smi" s" synth2.wav" ->mi add-sound smi
s" sfa" s" synth2.wav" ->fa add-sound sfa
s" ssol" s" synth2.wav" ->sol add-sound ssol
s" sla" s" synth2.wav" ->la add-sound sla
s" ssi" s" synth2.wav" ->si add-sound ssi
s" sdou" s" synth2.wav" ->do add-sound sdou

s" sre-" s" synth2.wav" <-re add-sound sre-
s" smi-" s" synth2.wav" <-mi add-sound smi-
s" sfa-" s" synth2.wav" <-fa add-sound sfa-
s" ssol-" s" synth2.wav" <-sol add-sound ssol-
s" sla-" s" synth2.wav" <-la add-sound sla-
s" ssi-" s" synth2.wav" <-si add-sound ssi-
s" sdou-" s" synth2.wav" <-do add-sound sdou-

SDLK_SPACE add-key bomb

SDLK_a     add-key sdo
SDLK_z     add-key sre
SDLK_e     add-key smi
SDLK_r     add-key sfa
SDLK_t     add-key ssol
SDLK_y     add-key sla
SDLK_u     add-key ssi
SDLK_i     add-key sdou

SDLK_q     add-key sdou-
SDLK_s     add-key sre-
SDLK_d     add-key smi-
SDLK_f     add-key sfa-
SDLK_g     add-key ssol-
SDLK_h     add-key sla-
SDLK_j     add-key ssi-




\ : patern1 ( --)
\     bomb __ bomb ____ bomb __ ;
\
\
\ 500 tempo
\
\ start-rec
\
\ sdou-  _
\ sre-  _
\ smi-  _
\ sfa-  _
\ ssol- _
\ sla-  _
\ ssi-  _
\
\ sdo  _
\ sre  _
\ smi  _
\ sfa  _
\ ssol _
\ sla  _
\ ssi  _
\ sdou _
\




\ start-rec
\
\ patern1 patern1 patern1
\
\ 500 tempo
\
\ bomb synth _ bomb _ _ bomb _
\ bomb _ bomb synth _ _ bomb _
\ bomb _ bomb _ _ synth bomb _
\
\ bomb _ bomb synth _ _ bomb _
\ bomb _ bomb _ _ bomb _
\ bomb _ bomb _ _ bomb _
\
\ 5 SEC

500 tempo

start-rec

key-loop

stop-rec

close

