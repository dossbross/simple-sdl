\ Test
include simple-sdl.fs

800 600 0 sdl-init

s" gnome-emacs.png" :image gnome
s" ball.png" :image ball

s" bomb.wav" :sound bomb

: test
  z" font/Vera.ttf" 30 sdl-open-font drop

  5 sdl-set-sound-volume
  bomb sdl-play-sound

  begin
	20 130 30 sdl-background
	gnome  100 100 sdl-put-image
	ball   200 100 sdl-put-image
	z" Un test en Forth!!!" 200 200 $EE0033 sdl-put-text

	sdl-poll-event
	\ ." x=" sdl-mouse-x . ." y=" sdl-mouse-y . cr
	\ ." button 1=" 1 sdl-mouse-button .
	\ ." button 2=" 2 sdl-mouse-button .
	\ ." button 3=" 3 sdl-mouse-button . cr
	SDLK_a sdl-key-press if ." a pressed!" cr then
	SDLK_b sdl-key-press if bomb sdl-play-sound then
	SDLK_UP sdl-key-press if ." UPPUPUPUPU" cr then

	sdl-present

	10 sdl-delay
	sdl-done?
  until
;

test

sdl-close

bye
