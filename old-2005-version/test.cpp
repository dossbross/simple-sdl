// build with:
// g++ `sdl-config --cflags --libs` -I.  -L. -lcl-simple-sdl -o test test.cpp
#include <stdio.h>
#include <stdlib.h>
#include <simple-sdl.h>

class test_t
{
public:
  int x;
};


int
main (int argc, char ** argv)
{
  test_t t;

  t.x = 15;

  printf ("Plop: %d\n", t.x);

  testfun (10);

  return 0;
}
