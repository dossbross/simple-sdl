;;;; -*- Mode: Lisp -*-
;;;; Author: Philippe Brochard <hocwp@free.fr>
;;;; ASDF System Definition
;;;
;;; #date#: Sun Nov 20 22:13:40 2005

(in-package #:asdf)

(defsystem cl-simple-sdl
    :description "CL-SPGUI - A Common Lisp Simple Portable GUI"
    :version "0.1"
    :author "Philippe Brochard  <hocwp@free.fr>"
    :licence"GNU Public License (GPL)"
    :components ((:file "cl-simple-sdl"))
    :depends-on (#+(not CLISP) :uffi))







