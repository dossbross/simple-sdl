include lib.fs
include random.fs

\ We need to make some C strings, so we include the forth to c
\ routine from above:
: ftoc ( c-addr u -- 'c-addr )
    here dup >r over allot swap cmove 0 c, align r> ;

: z" ( -- )  postpone s" postpone ftoc ; immediate

library libsdl ./libsimple-sdl.so

libsdl sdl-init ptr int int int int (void) sdl_init
libsdl sdl-close (void) sdl_close

libsdl sdl-show-cursor (void) sdl_show_cursor
libsdl sdl-hide-cursor (void) sdl_hide_cursor

libsdl sdl-testfun int (int) testfun

libsdl sdl-poll-event int (void) sdl_poll_event
libsdl sdl-wait-event (void) sdl_wait_event
libsdl sdl-wait-event-no-motion (void) sdl_wait_event_no_motion

libsdl sdl-flip-screen (void) sdl_flip_screen

libsdl sdl-key-press int (int) sdl_key_press
libsdl sdl-key-press+reset int (int) sdl_key_press_reset

libsdl sdl-mouse-button int (int) sdl_mouse_button
libsdl sdl-mouse-x (int) sdl_get_mouse_x
libsdl sdl-mouse-y (int) sdl_get_mouse_y

: sdl-mouse ( -- x y) sdl-mouse-x sdl-mouse-y ;

libsdl sdl-set-background int (void) sdl_set_background_color
libsdl sdl-show-background (void) sdl_show_background

libsdl sdl-load-image ptr (int) sdl_load_image
libsdl sdl-put-image int int int (void) sdl_put_image

libsdl sdl-open-font ptr int (int) sdl_open_font
libsdl sdl-put-text ptr int int int (void) sdl_put_text
libsdl sdl-text-width ptr (int) sdl_text_width
libsdl sdl-text-height ptr (int) sdl_text_height

libsdl sdl-put-line int int int int int (void) sdl_put_line

libsdl sdl-load-sound ptr (int) sdl_load_sound
libsdl sdl-play-sound int (void) sdl_play_sound
libsdl sdl-halt-sound (void) sdl_halt_sound
libsdl sdl-set-sound-volume int (void) sdl_set_sound_volume

libsdl sdl-get-ticks (int) sdl_get_ticks


\ Convertion from include/SDL/SDL_keysym.h
0 CONSTANT SDLK_UNKNOWN
0 CONSTANT SDLK_FIRST
8 CONSTANT SDLK_BACKSPACE
9 CONSTANT SDLK_TAB
12 CONSTANT SDLK_CLEAR
13 CONSTANT SDLK_RETURN
19 CONSTANT SDLK_PAUSE
27 CONSTANT SDLK_ESCAPE
32 CONSTANT SDLK_SPACE
33 CONSTANT SDLK_EXCLAIM
34 CONSTANT SDLK_QUOTEDBL
35 CONSTANT SDLK_HASH
36 CONSTANT SDLK_DOLLAR
38 CONSTANT SDLK_AMPERSAND
39 CONSTANT SDLK_QUOTE
40 CONSTANT SDLK_LEFTPAREN
41 CONSTANT SDLK_RIGHTPAREN
42 CONSTANT SDLK_ASTERISK
43 CONSTANT SDLK_PLUS
44 CONSTANT SDLK_COMMA
45 CONSTANT SDLK_MINUS
46 CONSTANT SDLK_PERIOD
47 CONSTANT SDLK_SLASH
48 CONSTANT SDLK_0
49 CONSTANT SDLK_1
50 CONSTANT SDLK_2
51 CONSTANT SDLK_3
52 CONSTANT SDLK_4
53 CONSTANT SDLK_5
54 CONSTANT SDLK_6
55 CONSTANT SDLK_7
56 CONSTANT SDLK_8
57 CONSTANT SDLK_9
58 CONSTANT SDLK_COLON
59 CONSTANT SDLK_SEMICOLON
60 CONSTANT SDLK_LESS
61 CONSTANT SDLK_EQUALS
62 CONSTANT SDLK_GREATER
63 CONSTANT SDLK_QUESTION
64 CONSTANT SDLK_AT

\ Skip uppercase letters
91 CONSTANT SDLK_LEFTBRACKET
92 CONSTANT SDLK_BACKSLASH
93 CONSTANT SDLK_RIGHTBRACKET
94 CONSTANT SDLK_CARET
95 CONSTANT SDLK_UNDERSCORE
96 CONSTANT SDLK_BACKQUOTE
97 CONSTANT SDLK_a
98 CONSTANT SDLK_b
99 CONSTANT SDLK_c
100 CONSTANT SDLK_d
101 CONSTANT SDLK_e
102 CONSTANT SDLK_f
103 CONSTANT SDLK_g
104 CONSTANT SDLK_h
105 CONSTANT SDLK_i
106 CONSTANT SDLK_j
107 CONSTANT SDLK_k
108 CONSTANT SDLK_l
109 CONSTANT SDLK_m
110 CONSTANT SDLK_n
111 CONSTANT SDLK_o
112 CONSTANT SDLK_p
113 CONSTANT SDLK_q
114 CONSTANT SDLK_r
115 CONSTANT SDLK_s
116 CONSTANT SDLK_t
117 CONSTANT SDLK_u
118 CONSTANT SDLK_v
119 CONSTANT SDLK_w
120 CONSTANT SDLK_x
121 CONSTANT SDLK_y
122 CONSTANT SDLK_z
127 CONSTANT SDLK_DELETE
\ End of ASCII mapped keysyms

\ International keyboard syms
160 CONSTANT SDLK_WORLD_0		\ 0xA0
161 CONSTANT SDLK_WORLD_1
162 CONSTANT SDLK_WORLD_2
163 CONSTANT SDLK_WORLD_3
164 CONSTANT SDLK_WORLD_4
165 CONSTANT SDLK_WORLD_5
166 CONSTANT SDLK_WORLD_6
167 CONSTANT SDLK_WORLD_7
168 CONSTANT SDLK_WORLD_8
169 CONSTANT SDLK_WORLD_9
170 CONSTANT SDLK_WORLD_10
171 CONSTANT SDLK_WORLD_11
172 CONSTANT SDLK_WORLD_12
173 CONSTANT SDLK_WORLD_13
174 CONSTANT SDLK_WORLD_14
175 CONSTANT SDLK_WORLD_15
176 CONSTANT SDLK_WORLD_16
177 CONSTANT SDLK_WORLD_17
178 CONSTANT SDLK_WORLD_18
179 CONSTANT SDLK_WORLD_19
180 CONSTANT SDLK_WORLD_20
181 CONSTANT SDLK_WORLD_21
182 CONSTANT SDLK_WORLD_22
183 CONSTANT SDLK_WORLD_23
184 CONSTANT SDLK_WORLD_24
185 CONSTANT SDLK_WORLD_25
186 CONSTANT SDLK_WORLD_26
187 CONSTANT SDLK_WORLD_27
188 CONSTANT SDLK_WORLD_28
189 CONSTANT SDLK_WORLD_29
190 CONSTANT SDLK_WORLD_30
191 CONSTANT SDLK_WORLD_31
192 CONSTANT SDLK_WORLD_32
193 CONSTANT SDLK_WORLD_33
194 CONSTANT SDLK_WORLD_34
195 CONSTANT SDLK_WORLD_35
196 CONSTANT SDLK_WORLD_36
197 CONSTANT SDLK_WORLD_37
198 CONSTANT SDLK_WORLD_38
199 CONSTANT SDLK_WORLD_39
200 CONSTANT SDLK_WORLD_40
201 CONSTANT SDLK_WORLD_41
202 CONSTANT SDLK_WORLD_42
203 CONSTANT SDLK_WORLD_43
204 CONSTANT SDLK_WORLD_44
205 CONSTANT SDLK_WORLD_45
206 CONSTANT SDLK_WORLD_46
207 CONSTANT SDLK_WORLD_47
208 CONSTANT SDLK_WORLD_48
209 CONSTANT SDLK_WORLD_49
210 CONSTANT SDLK_WORLD_50
211 CONSTANT SDLK_WORLD_51
212 CONSTANT SDLK_WORLD_52
213 CONSTANT SDLK_WORLD_53
214 CONSTANT SDLK_WORLD_54
215 CONSTANT SDLK_WORLD_55
216 CONSTANT SDLK_WORLD_56
217 CONSTANT SDLK_WORLD_57
218 CONSTANT SDLK_WORLD_58
219 CONSTANT SDLK_WORLD_59
220 CONSTANT SDLK_WORLD_60
221 CONSTANT SDLK_WORLD_61
222 CONSTANT SDLK_WORLD_62
223 CONSTANT SDLK_WORLD_63
224 CONSTANT SDLK_WORLD_64
225 CONSTANT SDLK_WORLD_65
226 CONSTANT SDLK_WORLD_66
227 CONSTANT SDLK_WORLD_67
228 CONSTANT SDLK_WORLD_68
229 CONSTANT SDLK_WORLD_69
230 CONSTANT SDLK_WORLD_70
231 CONSTANT SDLK_WORLD_71
232 CONSTANT SDLK_WORLD_72
233 CONSTANT SDLK_WORLD_73
234 CONSTANT SDLK_WORLD_74
235 CONSTANT SDLK_WORLD_75
236 CONSTANT SDLK_WORLD_76
237 CONSTANT SDLK_WORLD_77
238 CONSTANT SDLK_WORLD_78
239 CONSTANT SDLK_WORLD_79
240 CONSTANT SDLK_WORLD_80
241 CONSTANT SDLK_WORLD_81
242 CONSTANT SDLK_WORLD_82
243 CONSTANT SDLK_WORLD_83
244 CONSTANT SDLK_WORLD_84
245 CONSTANT SDLK_WORLD_85
246 CONSTANT SDLK_WORLD_86
247 CONSTANT SDLK_WORLD_87
248 CONSTANT SDLK_WORLD_88
249 CONSTANT SDLK_WORLD_89
250 CONSTANT SDLK_WORLD_90
251 CONSTANT SDLK_WORLD_91
252 CONSTANT SDLK_WORLD_92
253 CONSTANT SDLK_WORLD_93
254 CONSTANT SDLK_WORLD_94
255 CONSTANT SDLK_WORLD_95		\ 0xFF

\ Numeric keypad
256 CONSTANT SDLK_KP0
257 CONSTANT SDLK_KP1
258 CONSTANT SDLK_KP2
259 CONSTANT SDLK_KP3
260 CONSTANT SDLK_KP4
261 CONSTANT SDLK_KP5
262 CONSTANT SDLK_KP6
263 CONSTANT SDLK_KP7
264 CONSTANT SDLK_KP8
265 CONSTANT SDLK_KP9
266 CONSTANT SDLK_KP_PERIOD
267 CONSTANT SDLK_KP_DIVIDE
268 CONSTANT SDLK_KP_MULTIPLY
269 CONSTANT SDLK_KP_MINUS
270 CONSTANT SDLK_KP_PLUS
271 CONSTANT SDLK_KP_ENTER
272 CONSTANT SDLK_KP_EQUALS

\ Arrows + Home/End pad
273 CONSTANT SDLK_UP
274 CONSTANT SDLK_DOWN
275 CONSTANT SDLK_RIGHT
276 CONSTANT SDLK_LEFT
277 CONSTANT SDLK_INSERT
278 CONSTANT SDLK_HOME
279 CONSTANT SDLK_END
280 CONSTANT SDLK_PAGEUP
281 CONSTANT SDLK_PAGEDOWN

\ Function keys
282 CONSTANT SDLK_F1
283 CONSTANT SDLK_F2
284 CONSTANT SDLK_F3
285 CONSTANT SDLK_F4
286 CONSTANT SDLK_F5
287 CONSTANT SDLK_F6
288 CONSTANT SDLK_F7
289 CONSTANT SDLK_F8
290 CONSTANT SDLK_F9
291 CONSTANT SDLK_F10
292 CONSTANT SDLK_F11
293 CONSTANT SDLK_F12
294 CONSTANT SDLK_F13
295 CONSTANT SDLK_F14
296 CONSTANT SDLK_F15

\ Key state modifier keys
300 CONSTANT SDLK_NUMLOCK
301 CONSTANT SDLK_CAPSLOCK
302 CONSTANT SDLK_SCROLLOCK
303 CONSTANT SDLK_RSHIFT
304 CONSTANT SDLK_LSHIFT
305 CONSTANT SDLK_RCTRL
306 CONSTANT SDLK_LCTRL
307 CONSTANT SDLK_RALT
308 CONSTANT SDLK_LALT
309 CONSTANT SDLK_RMETA
310 CONSTANT SDLK_LMETA
311 CONSTANT SDLK_LSUPER		\ Left "Windows" key
312 CONSTANT SDLK_RSUPER		\ Right "Windows" key
313 CONSTANT SDLK_MODE		        \ "Alt Gr" key
314 CONSTANT SDLK_COMPOSE		\ Multi-key compose key

\ Miscellaneous function keys
315 CONSTANT SDLK_HELP
316 CONSTANT SDLK_PRINT
317 CONSTANT SDLK_SYSREQ
318 CONSTANT SDLK_BREAK
319 CONSTANT SDLK_MENU
320 CONSTANT SDLK_POWER		\ Power Macintosh power key
321 CONSTANT SDLK_EURO		\ Some european keyboards
322 CONSTANT SDLK_UNDO		\ Atari keyboard has Undo

\ Add any other keys here



: sdl-test ( --)
    z" toto" 800 600 0 10000 sdl-init

    $0F990F sdl-set-background
    sdl-show-background

    z" gnome-emacs.png" sdl-load-image \ leave image number on the stack
    dup 300 100 sdl-put-image

    z" font/Vera.ttf" 30 sdl-open-font drop

    z" Un test en Forth!!!" 200 200 $FF00FF sdl-put-text

    z" Toto" sdl-text-width .
    z" Toto" sdl-text-height .

    ." Ticks=" sdl-get-ticks .

    100 100 500 400 0 sdl-put-line

    z" bomb.wav" sdl-load-sound
    dup sdl-play-sound

    sdl-hide-cursor

    sdl-flip-screen

    1000 MS

    sdl-show-background

    sdl-show-cursor

    ( image#) 600 100 sdl-put-image

    z" Press ctrl-a to stop" 100 400 $0F0FFF sdl-put-text

    sdl-flip-screen

    sdl-play-sound

    0  \ Leave the end condition on the stack
    begin
	sdl-wait-event-no-motion

	CR ." B1=" 1 sdl-mouse-button .
	." B2=" 2 sdl-mouse-button .
	." X=" sdl-mouse-x .
	." Y=" sdl-mouse-y .
	." X,Y=" sdl-mouse swap . .

        SDLK_a sdl-key-press  SDLK_LCTRL sdl-key-press and if drop 1 then
	10 MS
	dup
    until
    drop

    sdl-close

    CR ." Final stack=" .s CR
;


\
\ Layer definition
\
variable couleur   $AA0055 couleur !
variable fin?     false fin? !
variable temps-attente  10

: flip sdl-flip-screen ;
: clear sdl-show-background ;
: clear!  ( bg-color --) sdl-set-background  sdl-show-background ;

: debut
    z" toto" 800 600 0 10000 sdl-init
    z" font/Vera.ttf" 30 sdl-open-font drop
    $0F990F sdl-set-background
    sdl-show-background
    flip
;

: fin  sdl-close ;

: image:  ( addr n --)
    ftoc sdl-load-image create ,
  does> @ ;

: son:    ( addr n --)
    ftoc sdl-load-sound create ,
  does> @ ;

: affiche  ( img x y)   sdl-put-image ;
: img!     ( img x y)   sdl-put-image ;
: aff      ( img x y)   sdl-put-image ;

: play ( addr --) sdl-play-sound ;
: joue ( addr --) sdl-play-sound ;


: texte ( x y text) 2swap ftoc -rot  couleur @ sdl-put-text ;
: �crit texte ;
: ecrit texte ;


: press?   sdl-key-press ;
: button?  sdl-mouse-button ;

: souris?  sdl-mouse ;
: souris-x?  sdl-mouse-x ;
: souris-y?  sdl-mouse-y ;


defer touche-quitte? ( --)

: (touche-quitte?)
    SDLK_q sdl-key-press  SDLK_LCTRL sdl-key-press and if true fin? ! then ;

' (touche-quitte?) is touche-quitte?

defer actions ( --)

: (actions) ;

' (actions) is actions

defer attend ( --)

' sdl-wait-event-no-motion is attend

: attend-temps  temps-attente @ sdl-poll-event ;

: boucle
    begin attend  actions  touche-quitte?   fin? @  until ;

